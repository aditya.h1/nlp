#!/usr/bin/env python
# coding: utf-8

# In[1]:


#We'll take a dataset of documents in several different categories, 
#and find topics (consisting of groups of words) for them. 
#Knowing the actual categories helps us evaluate if the topics we find make sense.

#We will try this with two different matrix factorizations: **Singular Value Decomposition (SVD)
#** and **Non-negative Matrix Factorization (NMF)**

import numpy as np
from sklearn.datasets import fetch_20newsgroups
from sklearn import decomposition
from scipy import linalg
import matplotlib.pyplot as plt


# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')
np.set_printoptions(suppress=True)


# In[3]:


#we will be using the newsgroups dataset from sklearn
categories = ['alt.atheism', 'talk.religion.misc', 'comp.graphics', 'sci.space']
remove = ('headers', 'footers', 'quotes')
newsgroups_train = fetch_20newsgroups(subset='train', categories=categories, remove=remove)
newsgroups_test = fetch_20newsgroups(subset='test', categories=categories, remove=remove)


# In[4]:


newsgroups_train.filenames.shape, newsgroups_train.target.shape


# In[5]:


print("\n".join(newsgroups_train.data[:3]))


# In[6]:


np.array(newsgroups_train.target_names)[newsgroups_train.target[:3]]


# In[7]:


newsgroups_train.target[:10]


# In[8]:


num_topics, num_top_words = 6, 8


# In[9]:


#Stopwords, stemming and Lemmatization

from sklearn.feature_extraction import stop_words

sorted(list(stop_words.ENGLISH_STOP_WORDS))[:20]


# In[10]:


import nltk
nltk.download('wordnet')


# In[11]:


from nltk import stem


# In[12]:


wnl = stem.WordNetLemmatizer()
porter = stem.porter.PorterStemmer()


# In[13]:


word_list = ['feet', 'foot', 'foots', 'footing']


# In[14]:


[wnl.lemmatize(word) for word in word_list]


# In[15]:


[porter.stem(word) for word in word_list]


# In[16]:


#Using another library Spacy
import spacy

import en_core_web_sm
nlp = en_core_web_sm.load()


# In[17]:


from spacy.lemmatizer import Lemmatizer
from spacy.lookups import Lookups
lookups = Lookups()
lemmatizer = Lemmatizer(lookups)


# In[18]:


[lemmatizer.lookup(word) for word in word_list]


# In[19]:


sorted(list(nlp.Defaults.stop_words))[:20]


# In[21]:


#Data Processing using sklearn
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import nltk


# In[22]:


vectorizer = CountVectorizer(stop_words='english') #, tokenizer=LemmaTokenizer())


# In[23]:


vectors = vectorizer.fit_transform(newsgroups_train.data).todense() # (documents, vocab)
vectors.shape #, vectors.nnz / vectors.shape[0], row_means.shape


# In[24]:


print(len(newsgroups_train.data), vectors.shape)


# In[25]:


vocab = np.array(vectorizer.get_feature_names())


# In[26]:


vocab.shape


# In[27]:


vocab[7000:7020]


# In[28]:


#Singular Value Decomposition
get_ipython().run_line_magic('time', 'U, s, Vh = linalg.svd(vectors, full_matrices=False)')


# In[29]:


print(U.shape, s.shape, Vh.shape)


# In[30]:


s[:4]


# In[31]:


np.diag(np.diag(s[:4]))


# In[32]:


plt.plot(s);


# In[33]:


plt.plot(s[:10])


# In[34]:


num_top_words=8

def show_topics(a):
    top_words = lambda t: [vocab[i] for i in np.argsort(t)[:-num_top_words-1:-1]]
    topic_words = ([top_words(t) for t in a])
    return [' '.join(t) for t in topic_words]


# In[35]:


show_topics(Vh[:10])


# In[36]:


#Non Negative Matrix Factorisation
#We will be using NMF with SKLearn
m,n=vectors.shape
d=5  # num topics


# In[37]:


clf = decomposition.NMF(n_components=d, random_state=1)

W1 = clf.fit_transform(vectors)
H1 = clf.components_


# In[38]:


show_topics(H1)


# In[39]:


#Topic Frequency-Inverse Document Frequency (TF-IDF) is a way to 
#normalize term counts by taking into account how often they appear in a document,
#how long the document is, and how commmon/rare the term is.
vectorizer_tfidf = TfidfVectorizer(stop_words='english')
vectors_tfidf = vectorizer_tfidf.fit_transform(newsgroups_train.data) # (documents, vocab)


# In[40]:


newsgroups_train.data[10:20]


# In[41]:


W1 = clf.fit_transform(vectors_tfidf)
H1 = clf.components_


# In[42]:


show_topics(H1)


# In[43]:


plt.plot(clf.components_[0])


# In[44]:


clf.reconstruction_err_


# In[ ]:




